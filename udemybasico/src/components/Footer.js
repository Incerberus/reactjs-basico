import React from "react";

const owner = "Francisco Montenegro";
const year_expiracy = new Date().getFullYear();

const Footer = () => (
  <h3>
    Copyrights to {owner} - {year_expiracy}
  </h3>
);

export default Footer;
